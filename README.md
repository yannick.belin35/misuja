Misuja: Midi Sequencer Using Jack
=================================

A library to drive the MIDI system of the Jack Audio Connection Kit.

Misuja is a low-latency “MIDI communications thread” implemented in C
which is manipulated with an OCaml API:
[`Misuja.Sequencer`](https://smondet.gitlab.io/misuja/misuja/Misuja/Sequencer/index.html)
(the process communicates with the Sequencer thread through ring-buffers
provided by the Jack API).

This library is extracted from the venerable
[Locoseq](https://github.com/smondet/locoseq).


Build/Install
-------------

To build/install the library:

    opam pin add misuja . -kgit

or to build locall:

    dune build @install

The library requires the development files of the JACK library, see the
`depexts` field in the `misuja.opam` file; the package is named
[`jack-audio-connection-kit-devel`](https://pkgs.org/download/jack-audio-connection-kit-devel)
on most RPM-based distributions, and
[`libjack-jackd2-dev`](https://packages.ubuntu.com/xenial/libjack-jackd2-dev) on
Ubuntu/Debian.


On Nix, this may look like:

    nix-shell -p pkg-config -p libjack2 --run 'dune build @install src/test/relay.exe'

Tests
-----

Build them:

    dune build @install src/test/relay.exe
    
then:

    _build/default/src/test/relay.exe
    
The test opens a Jack-midi client with 10 input and 10 output ports; it just
relays everything it gets as input but in the case of “note-on/off” MIDI events
it “power-harmonizes” them (it makes just “power chords,” i.e. it adds fifths
and octaves).

Here is a setup with `a2jmidid`, `vmpk`, QSynth (in Jack-midi mode) connected
with QJackCtl:

<div><a href="https://cloud.githubusercontent.com/assets/617111/25602378/5af5adc4-2ec1-11e7-8731-5e911a2adf94.png"><img
 width="80%"
  src="https://cloud.githubusercontent.com/assets/617111/25602378/5af5adc4-2ec1-11e7-8731-5e911a2adf94.png"
></a></div>
